package udep.ing.poo.myagenda2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

public class SignupActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        botonRegistro = (Button)findViewById(R.id.button3);
        botonRegistro.setOnClickListener(this);
        editUsuarioR = (EditText)findViewById((R.id.editText3));
        editConstrasenaR = (EditText)findViewById(R.id.editText4);
        editConstrasenaR2 = (EditText)findViewById(R.id.editText5);

        final Switch switchDark = findViewById(R.id.switch3);
        SharedPreferences sharedPreferences = getSharedPreferences("save",MODE_PRIVATE);
        switchDark.setChecked(sharedPreferences.getBoolean("value",true));

        if(sharedPreferences.getBoolean("value",true)){
            SharedPreferences.Editor editor = getSharedPreferences("save",MODE_PRIVATE).edit();
            editor.putBoolean("value",true);
            editor.apply();
            switchDark.setChecked(true);
            getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }else{
            SharedPreferences.Editor editor = getSharedPreferences("save",MODE_PRIVATE).edit();
            editor.putBoolean("value",false);
            editor.apply();
            switchDark.setChecked(false);
            getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        switchDark.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    SharedPreferences.Editor editor = getSharedPreferences("save",MODE_PRIVATE).edit();
                    editor.putBoolean("value",true);
                    editor.apply();
                    switchDark.setChecked(true);
                    getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                }else{
                    SharedPreferences.Editor editor = getSharedPreferences("save",MODE_PRIVATE).edit();
                    editor.putBoolean("value",false);
                    editor.apply();
                    switchDark.setChecked(false);
                    getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                }
            }
        });
    }

    Button botonRegistro;
    EditText editUsuarioR, editConstrasenaR, editConstrasenaR2;

    @Override
    public void onClick(View v) {


        if  (v.getId()==R.id.button3){

            String inputUsuarioR = editUsuarioR.getText().toString();
            String inputContrasenaR = editConstrasenaR.getText().toString();
            String inputContrasenaR2 = editConstrasenaR2.getText().toString();

            if (inputUsuarioR.equals("")){
                //usuario está en blanco
                Toast.makeText(this,getString(R.string.alertaUsuarioBlanco),Toast.LENGTH_SHORT).show();

            }else if (inputContrasenaR.equals("") || inputContrasenaR2.equals("")){
                //contraseñas en blanco
                Toast.makeText(this,getString(R.string.alertaContrasenaBlanco),Toast.LENGTH_SHORT).show();
            }else if (!inputContrasenaR.equals(inputContrasenaR2)){
                //contraseñas no coinciden
                Toast.makeText(this,getString(R.string.alertaContraseñasDiferentes),Toast.LENGTH_SHORT).show();

            } else {

                Usuarios usuarios = new Usuarios(this.getSharedPreferences("MyAgenda2",Context.MODE_PRIVATE));

                usuarios.guardar(inputUsuarioR,inputContrasenaR);
                usuarios.escribirUsuarios();

                Intent intent = new Intent(this,MainActivity.class);
                intent.putExtra("usuario", inputUsuarioR);
                startActivity(intent);

        }
        }

    }
}
